## Mail-Mock-Bit
To start the project you can use `npm start` (see below, I've left the original useful commands from Create React App).

Everything in the project is done trying to take in consideration best practices and code quality.

This is the best I was able to achieve respecting the 5 days deadline, but please, 
take in consideration I've never worked in React before (only a daily hackaton as test assignment for my current job).

All the requirements should be respected as well as one optional condition. 

## Functionalities
* Read/Unread emails
* Open emails in the right pane
* Switch Account using the top-right menu
* Search bar searches for queries emails subject

## Limitations and known issues
* Emails list design was a bit confusing so I implemented the state visible in the mock-up as `selected` 
and implemented a small dot to indicate that the mail is not read yet. My mistake for not noticing before.
* Every other section (but inbox) and compose button of the app is linked to a different path 
which, unfortunately, does not contain any component
* Mark as spam, favorite, unread and move to the bin buttons don't perform any action
* Navigation through emails, using the right pane buttons, doesn't work
* Methods documentation and tests are not 100% complete.


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
