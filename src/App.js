import React, {useState} from 'react';
import {Redirect, Router, Route, Switch} from "react-router-dom";
import {createBrowserHistory} from 'history';
import ClientInboxSection from "./components/ClientInboxSection/ClientInboxSection";
import NavSidePanel from "./components/NavSidePanel/NavSidePanel";
import Toolbar from "./components/Toolbar/Toolbar"
import mockedData from './mockedData';
import './App.scss';

const App = () => {
    const history = createBrowserHistory();

    // Setting the first account as default selected
    const [currentAccount, setCurrentAccount] = useState(mockedData.accounts[0]);

    // Storing the original content of account mails
    const originalMails = currentAccount.mail;

    /**
     * Set the account mail property according to the value in filteredMails.
     * @param filteredMails
     */
    const onSearchEmails = (filteredMails) => {
        let filteredMailAccount;

        if (filteredMails) {
            filteredMailAccount = {...currentAccount, mail: filteredMails};
        }
        else {
            filteredMailAccount = {...currentAccount, mail: originalMails};
        }

        return setCurrentAccount(filteredMailAccount);
    };

    /**
     * Set the newly selectedAccount as currentAccount
     * @param account
     */
    const onSwitchAccount = (account) => {
        setCurrentAccount(account);
    };

    return (
        <div className="App">
            <div className="app-navigation">
                <NavSidePanel history={history}/>
            </div>

            <div className="app-content">
                <Toolbar account={currentAccount} accounts={mockedData.accounts} onSwitchAccount={onSwitchAccount} onSearchEmails={onSearchEmails}/>

                <Router history={history}>
                    <Switch>
                        <Route exact path="/">
                            <Redirect to="/inbox" />
                        </Route>
                        <Route path="/inbox" render={(props) => <ClientInboxSection {...props} currentAccount={currentAccount} />} />
                        <Route path="/drafts" render={() => ''} />
                        <Route path="/compose" render={() => ''} />
                        <Route path="/sent" render={() => ''} />
                        <Route path="/spam" render={() => ''} />
                        <Route path="/trash" render={() => ''} />
                    </Switch>
                </Router>
            </div>
        </div>
    );
};

export default App;
