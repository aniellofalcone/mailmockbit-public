import React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import {createBrowserHistory} from 'history';
import {Router} from "react-router-dom";

import ClientInboxSection from "./ClientInboxSection";

import mockedData from '../../mockedData'

let container = null;
const history = createBrowserHistory();
const currentAccount = mockedData.accounts[0];

// setup a DOM element as a render target
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

// cleanup on exiting
afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders the component", () => {
    const clientInbox = document.getElementsByClassName('container-inbox');
    const containerList = document.getElementsByClassName('container-list');
    const listItem = document.getElementsByClassName('container-item-details');

    act(() => {
        render(
            <Router history={history}>
                <ClientInboxSection currentAccount={currentAccount} />
            </Router>,
            container
        );
    });

    expect(clientInbox).toBeDefined();
    expect(containerList).toBeDefined();
    expect(listItem).toBeDefined();
});
