import React, {useEffect, useState} from "react";
import ActionsBar from "../ActionsBar/ActionsBar";
import MailItemDetail from "../MailItemDetail/MailItemDetail"
import MailboxList from "../MailboxList/MailboxList";
import './ClientInboxSection.scss'

export const MailContext = React.createContext('');

const ClientInboxSection = (props) => {
    const { currentAccount } = props;
    const [selectedEmail, setSelectedEmail] = useState();

    /**
     * Defining the actions available via the context
     * @type {{setSelectedEmail: (function(*=): void), currentAccount: *, selectedEmail: *}}
     */
    const contextActions = {
        currentAccount,
        selectedEmail,
        setSelectedEmail: (mail) => setSelectedEmail(mail)
    };

    /**
     * Handling the side effect when component get re-instantiated.
     * As depes it has only the currentAccount,
     * since there is no need to trigger this when the selectedEmail changes.
     */
    useEffect(() => {
        if (selectedEmail) {
            setSelectedEmail(null);
        }
    }, [currentAccount]);

    return (
        <MailContext.Provider value={contextActions}>
            <div className="container-inbox">
                <div className="container-inbox-list">
                    <ActionsBar />

                    <MailboxList mails={currentAccount.mail}/>
                </div>

                {selectedEmail && <MailItemDetail selectedEmail={selectedEmail}/>}
            </div>
        </MailContext.Provider>
    )
};

export default ClientInboxSection;
