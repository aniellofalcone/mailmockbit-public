import React, {useState} from "react";
import {MdPerson, MdSearch} from "react-icons/all";
import './Toolbar.scss'

const Toolbar = (props) => {
    const {account, accounts, onSearchEmails, onSwitchAccount} = props;
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [currentAccount, setCurrentAccount] = useState(account);

    /**
     * Handling the input event from the search input field and returns
     * a list of filtered mails to App.js
     * @param event
     * @return filteredMail[]
     */
    const handleInputChange = (event) => {
        const query = event.target.value;
        const filtered = currentAccount.mail.filter(mail => mail.subject.toLowerCase().indexOf(query) >= 0);
        onSearchEmails(filtered);
    };

    /**
     * Set isMenuOpen property on the state in order to show/hide the account menu
     */
    const openMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    /**
     * Handle the switch between accounts.
     * Set the selectedAccount as currentAccount and pass its value to App.js.
     * Then it closes the menu;
     * @param newAccount
     */
    const switchAccount = (newAccount) => {
        setCurrentAccount(newAccount);
        onSwitchAccount(newAccount);
        setIsMenuOpen(false);
    };

    return (
        <div className="app-toolbar">
            <div className="container-search">
                <form>
                    <MdSearch />
                    <input type="text" className="search-input" placeholder="Search" onChange={handleInputChange}/>
                </form>
            </div>

            <div className="container-account" onClick={openMenu}>
                <div className="account-avatar">
                    <MdPerson />
                </div>

                <div className="account-name">
                    {currentAccount.name} {currentAccount.surname}
                </div>
            </div>

            {isMenuOpen &&
                <div className="container-menu">
                    {accounts.map((a, i) => {
                        return (
                            <div key={`account-${i}`} className="accounts-menu-item" onClick={() => switchAccount(a)}>
                                <div className="item-name">
                                    {a.name} {a.surname}
                                </div>
                            </div>
                        )
                    })}
                </div>
            }
        </div>
    )
};

export default Toolbar;
