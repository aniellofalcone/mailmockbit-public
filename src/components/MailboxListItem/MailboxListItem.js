import React, {useEffect, useState} from "react";
import {withRouter} from "react-router-dom";
import Checkbox from "../Checkbox/Checkbox";
import {MailContext} from "../ClientInboxSection/ClientInboxSection";
import {dateSwitch} from "../../services/DateSwitch"
import {MdStar} from "react-icons/md";
import "./MailboxListItem.scss"

const MailboxListItem = ({mail}) => {
    const [isRead, setIsRead] = useState(false);

    const actions = React.useContext(MailContext);

    /**
     * Handles the click on each mail in the list.
     * Stops the propagation to let the user click on the Checkbox child component.
     * Then sets the selectedEmail using th context actions.
     * @param event
     */
    const handleOnClick = (event) => {
        event.stopPropagation();
        setIsRead(true);
        mail = {...mail, isRead: true};
        actions.setSelectedEmail(mail);
    };

    /**
     * Returns if the mail is also the selectedEmail, in order to change UI style
     * @returns {boolean}
     */
    const isSelected = () => {
        const selectedEmail = actions.selectedEmail;
        return selectedEmail ? mail.date === selectedEmail.date : false;
    };

    /**
     * Setting back to unread back when switching account
     */
    useEffect(() => {
        setIsRead(false);
    }, [mail]);

    return (
        <MailContext.Consumer>
            {() => {
                return (
                    <div className={`container-list-item ${isSelected() ? 'selected' : ''}`} onClick={(e) => handleOnClick(e)}>
                        <div className="container-checkbox">
                            <Checkbox/>
                        </div>

                        <div className="container-mail-details">
                            <div className="mail-subject">
                                {mail.subject}
                            </div>
                            <div className="mail-sender">
                                {mail['sender name'] ? mail['sender name'] : mail['sender email']}
                            </div>
                        </div>

                        <div className="container-date">
                            {dateSwitch(mail)}
                        </div>

                        <div className="container-favorite">
                            <MdStar/>
                        </div>

                        {isRead === false && <div className="container-read-indicator">
                            <div className="read-indicator" />
                        </div>}
                    </div>
                );
            }}
        </MailContext.Consumer>
    )
};

export default withRouter(MailboxListItem);
