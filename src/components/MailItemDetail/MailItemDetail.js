import React from "react";
import EmailNavigator from "../EmailNavigator/EmailNavigator";
import "./MailItemDetail.scss"
import EmailView from "../EmailView/EmailView";

const MailItemDetail = (props) => {
    const {selectedEmail} = props;

    return (
        <div className="container-item-details">
            <div className='container-item-toolbar'>
                <div className="email-subject">
                    {selectedEmail.subject}
                </div>

                <EmailNavigator />
            </div>

            <EmailView selectedEmail={selectedEmail}/>
        </div>
)
};

export default MailItemDetail;
