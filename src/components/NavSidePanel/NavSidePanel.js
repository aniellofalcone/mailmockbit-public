import React from "react";
import {Link, Router} from 'react-router-dom';

import {
    MdDelete,
    MdDrafts,
    MdInbox,
    MdMailOutline,
    MdRemoveCircle,
    MdSend
} from "react-icons/md";
import './NavSidePanel.scss';

const NavSidePanel = (props) => {
    const {history} = props;

    /**
     * Compose new message, redirects to '/compose';
     */
    const composeMessage = () => {
        history.push('/compose');
    };

    // Navigation panel configuration.
    // It's a object containing all the info needed to render the list using .map();
    const panelConfig = [
        {
            id: 'inbox',
            label: 'Inbox',
            icon: <MdInbox />
        },
        {
            id: 'draft',
            label: 'Draft',
            icon: <MdDrafts />
        },
        {
            id: 'sent',
            label: 'Sent',
            icon: <MdSend />
        },
        {
            id: 'spam',
            label: 'Spam',
            icon: <MdRemoveCircle />
        },
        {
            id: 'trash',
            label: 'Trash',
            icon: <MdDelete />
        },
    ];

    return (
        <div className="nav-side-container">
            <div className="nav-side-title">
                <MdMailOutline /> Mail
            </div>

            <div className="nav-side-compose">
                <button onClick={composeMessage}>
                    Compose
                </button>
            </div>

            <Router history={history}>
                {panelConfig.map(section => {
                    return (
                        <Link to={`/${section.id}`} key={section.id} className="nav-side-section">
                            <div className="section-icon">{section.icon}</div>
                            <div className="section-label">{section.label}</div>
                        </Link>
                    )
                })}
            </Router>
        </div>
    );
};

export default NavSidePanel;
