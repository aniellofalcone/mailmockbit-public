import React from "react";
import {dateSwitch} from "../../services/DateSwitch";
import {MailContext} from "../ClientInboxSection/ClientInboxSection";
import {MdStar} from "react-icons/md";
import * as sanitizeHtml from 'sanitize-html';
import "./EmailView.scss"

const EmailView = (props) => {
    const {selectedEmail} = props;
    const actions = React.useContext(MailContext);
    const fullName = `${actions.currentAccount.name} ${actions.currentAccount.surname}`;
    const sanitizedHtml = sanitizeHtml(selectedEmail.content, {
        allowedTags: sanitizeHtml.defaults.allowedTags.concat([ 'script' ])
    });

    return (
        <MailContext.Consumer>
            {() => {
                return (
                    <div className="container-email-view">
                        <div className="container-email-header">
                            <div className="email-from-to">
                                <div className="email-from">
                                    <div>From:</div>
                                    <div className="info-value">
                                        {selectedEmail['sender name']}
                                    </div>
                                </div>

                                <div className="email-to">
                                    <div>To:</div>
                                    <div className="info-value">{fullName}</div>
                                </div>
                            </div>

                            <div className="email-date-favorite">
                                <div className="date">
                                    {dateSwitch(selectedEmail)}
                                </div>

                                <div className="container-favorite">
                                    <MdStar/>
                                </div>
                            </div>
                        </div>

                        <div className="container-email-content" dangerouslySetInnerHTML={{__html: sanitizedHtml}}/>
                    </div>
                )
            }}
        </MailContext.Consumer>
    );
};

export default EmailView;
