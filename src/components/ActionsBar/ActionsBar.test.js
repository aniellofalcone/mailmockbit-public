import React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import ActionsBar from "./ActionsBar";

let container = null;

// setup a DOM element as a render target
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

// cleanup on exiting
afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders the component", () => {
    const actionBar = document.getElementsByClassName('actions-bar');

    act(() => {
        render(<ActionsBar/>, container);
    });

    expect(actionBar).toBeDefined();
});
