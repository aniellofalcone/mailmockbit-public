import React from "react";
import Checkbox from "../Checkbox/Checkbox";
import { IoMdEyeOff } from "react-icons/io";
import {MdDelete, MdRemoveCircleOutline} from "react-icons/md"
import './ActionsBar.scss'

const ActionsBar = () => {
    return (
        <div className="actions-bar">
            <div className="container-select-all">
                <form>
                    <Checkbox />
                </form>
            </div>

            <div className="container-actions">
                <div className="action">
                    <IoMdEyeOff />
                </div>

                <div className="action">
                    <MdDelete />
                </div>

                <div className="action">
                    <MdRemoveCircleOutline />
                </div>
            </div>
        </div>
    )
};

export default ActionsBar;
