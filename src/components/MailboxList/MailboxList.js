import React from "react";
import MailboxListItem from "../MailboxListItem/MailboxListItem";
import "./MailboxList.scss"

const MailboxList = (props) => {
    const {mails} = props;

    mails.sort((a, b) => b.date - a.date)
        .forEach(m => m['isRead'] = false);

    return (
        <div className="container-list">
            {mails.map((mail) => {
                return <MailboxListItem key={mail.date} mail={mail} />
            })}
        </div>
    )
};

export default MailboxList;
