import React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import {createBrowserHistory} from 'history';
import {Router} from "react-router-dom";

import MailboxList from "./MailboxList";

import mockedData from '../../mockedData'

const history = createBrowserHistory();
const currentAccount = mockedData.accounts[0];
let container = null;

// setup a DOM element as a render target
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

// cleanup on exiting
afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders the component", () => {
    act(() => {
        render(
            <Router history={history}>
                <MailboxList mails={currentAccount.mail}/>
            </Router>,
            container
        );
    });

    const mailBoxList = document.getElementsByClassName('container-list')[0];
    const mailBoxListItem = document.getElementsByClassName('container-list-item')[0];
    const readIndicator = document.getElementsByClassName('container-read-indicator')[0];

    expect(mailBoxList).toBeDefined();
    expect(mailBoxListItem).toBeDefined();
    expect(readIndicator).toBeDefined();
});
