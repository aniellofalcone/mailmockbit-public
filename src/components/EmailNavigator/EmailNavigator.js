import React from "react";
import {MdNavigateBefore, MdNavigateNext} from "react-icons/md";
import "./EmailNavigator.scss"

const EmailNavigator = () => {
    return (
        <div className="container-navigator">
            <div className="navigator-prev">
                <MdNavigateBefore/>
            </div>
            <div className="navigator-next">
                <MdNavigateNext/>
            </div>
        </div>
    )
};

export default EmailNavigator;
