import React from 'react';
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Checkbox from "./Checkbox";

let container = null;

// setup a DOM element as a render target
beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
});

// cleanup on exiting
afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
});

it("renders the component", () => {
    const checkBox = document.getElementsByClassName('container-checkbox-label');

    act(() => {
        render(<Checkbox/>, container);
    });

    expect(checkBox).toBeDefined();
});
