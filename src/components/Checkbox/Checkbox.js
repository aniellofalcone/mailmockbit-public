import React from "react";
import './Checkbox.scss'

const Checkbox = () => {
    return (
        <label className="container-checkbox-label">
            <input type="checkbox"/>
                <span className="check-mark" />
        </label>
    )
};

export default Checkbox;
