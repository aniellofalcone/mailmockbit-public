export const dateSwitch = (mail) => {
    const date = new Date(mail.date * 1000);
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const hour = date.getHours();
    const mins = date.getMinutes();
    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    const today = new Date();
    const formattedDate = `${months[month]} ${day}`;

    if (day === today.getDate()) {
        return (`${hour}:${mins}`);
    } else if (year < today.getFullYear()) {
        return (`${formattedDate}, ${year}`)
    } else {
        return formattedDate;
    }
};
